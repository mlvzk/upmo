package roles

import "github.com/asdine/storm"

var (
	db   *storm.DB
	node storm.Node
)

// Initialize initializes the logger
func Initialize(d *storm.DB) {
	db = d
	node = db.From("guild_roles")
}
