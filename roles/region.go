package roles

import "github.com/bwmarrin/discordgo"

// Region is the struct for a role region.
type Region struct {
	StartID   string `storm:"unique,id"`
	String    string
	AllowGive bool

	ChildrenIDs []string

	ptr *discordgo.Role
}

// GetRegions returns the stored regions for the guild. If
// ChildrenIDs is empty, you may need to run Update()
func GetRegions(guildID string) (t []*Region, e error) {
	var rg []*Region
	if err := node.From(guildID).All(&rg); err != nil {
		return nil, err
	}

	t = make([]*Region, 0, len(rg))
	for _, r := range rg {
		if r.AllowGive {
			t = append(t, r)
		}
	}

	return
}
