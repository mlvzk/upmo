package main

import (
	"errors"

	"github.com/Necroforger/dgrouter/exrouter"
	"gitlab.com/diamondburned/upmo/logger"
)

func routerLogger(ctx *exrouter.Context) {
	a := ctx.Args.Get(1)
	if a == "" {
		sendToDiscord(ctx, "Missing target channel")
		return
	}

	if a == "test" {
		logger.Error(
			d, ctx.Msg.GuildID,
			errors.New("This is a test error"),
		)

		return
	}

	c, err := ctx.Channel(a[2 : len(a)-1])
	if err != nil {
		sendToDiscord(ctx, "Failed getting channel: "+err.Error())
		return
	}

	if c.GuildID == "" {
		return
	}

	if err := logger.Setup(ctx.Ses, c.GuildID, c.ID); err != nil {
		errToDiscord(ctx, err)
		return
	}

	sendToDiscord(ctx, "Done.")
}
