#!/usr/bin/env bash
[[ $1 ]] && input=$(< "$1") || input=$(xclip -o -selection clipboard)
mkdir -p output/

while IFS="|" read -d $'\n' -r name url; do
	echo "Downloading $name.png"
    wget "$url" -O "output/$name.png"
done <<< "$input"

./org.sh
