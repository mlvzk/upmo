package main

import (
	"fmt"
	"strings"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/stalin"
)

func routerStalin(ctx *exrouter.Context) {
	switch ctx.Args.Get(1) {
	case "setup":
		if err := stalin.Setup(ctx.Ses, ctx.Msg.GuildID); err != nil {
			errToDiscord(ctx, err)
			return
		}

		sendToDiscord(ctx, "Done.")
	case "blacklist":
		if a := ctx.Args.After(2); a != "" {
			words := strings.Fields(ctx.Args.After(2))

			if err := stalin.AddBlacklistedWord(ctx.Ses, ctx.Msg.GuildID, words...); err != nil {
				errToDiscord(ctx, err)
				return
			}

			sendToDiscord(ctx, "Done.")
			return
		}

		words, err := stalin.GetBlacklistedWords(ctx.Ses, ctx.Msg.GuildID)
		if err != nil {
			errToDiscord(ctx, err)
			return
		}

		e := &discordgo.MessageEmbed{
			Title:       fmt.Sprintf("%d blacklisted words", len(words)),
			Description: strings.Join(words, "\n"),
		}

		sendToDiscord(ctx, e)
	case "remove":
		words := strings.Fields(ctx.Args.After(2))

		if err := stalin.RemoveBlacklistedWord(ctx.Ses, ctx.Msg.GuildID, words...); err != nil {
			errToDiscord(ctx, err)
			return
		}

		sendToDiscord(ctx, "Done.")
	case "except":
		if a := ctx.Args.Get(2); a != "" {
			if err := stalin.AddExceptionChannel(ctx.Ses, ctx.Msg.GuildID, a[2:len(a)-1]); err != nil {
				errToDiscord(ctx, err)
				return
			}

			sendToDiscord(ctx, "Done.")
			return
		}

		chs, err := stalin.GetExceptionChannels(ctx.Ses, ctx.Msg.GuildID)
		if err != nil {
			errToDiscord(ctx, err)
			return
		}

		e := &discordgo.MessageEmbed{
			Title:       fmt.Sprintf("%d exception channels", len(chs)),
			Description: strings.Join(chs, "\n"),
		}

		sendToDiscord(ctx, e)
	default:
		sendToDiscord(ctx, `Missing command:
	- setup
	- blacklist [(empty for list)|word...]
	- remove [word...]
	- except [channel]`)
	}

}
