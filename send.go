package main

import (
	"fmt"
	"runtime"
	"strings"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
)

func sendToDiscord(c *exrouter.Context, content interface{}) (m *discordgo.Message, e error) {
	switch content := content.(type) {
	case string:
		m, e = c.Ses.ChannelMessageSend(c.Msg.ChannelID, content)
	case *discordgo.MessageEmbed:
		m, e = c.Ses.ChannelMessageSendEmbed(c.Msg.ChannelID, content)
	case *discordgo.MessageSend:
		m, e = c.Ses.ChannelMessageSendComplex(c.Msg.ChannelID, content)
	default:
		m, e = c.Reply(content)
	}

	if e != nil {
		logger.Error(c.Ses, c.Msg.GuildID, e)
	}

	return m, e
}

func errToDiscord(c *exrouter.Context, err error) (m *discordgo.Message, e error) {
	s := strings.Builder{}

	s.WriteString("A server-side error has occured:\n```")

	switch err := err.(type) {
	case *discordgo.RESTError:
		if err.Message != nil && err.Message.Message != "" {
			fmt.Fprintf(&s, "%d: %s\n", err.Message.Code, err.Message.Message)
			break
		}

		s.WriteString(err.Error())
	default:
		s.WriteString(err.Error())
	}

	s.WriteString("```\nTrace:\n```")

	for i := 1; ; i++ {
		p, file, ln, ok := runtime.Caller(i)
		if !ok {
			break
		}

		d := runtime.FuncForPC(p)

		fileparts := strings.Split(file, "/")

		fmt.Fprintf(&s, "%s#L%d: %s()\n", fileparts[len(fileparts)-1], ln, d.Name())
	}

	s.WriteString("```")

	return c.Ses.ChannelMessageSend(c.Msg.ChannelID, s.String())
}
