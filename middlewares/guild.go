package middlewares

import "github.com/Necroforger/dgrouter/exrouter"

// GuildOnly only allows messages that are from guilds, not
// direct messages
func GuildOnly(h exrouter.HandlerFunc) exrouter.HandlerFunc {
	return func(ctx *exrouter.Context) {
		if ctx.Msg.GuildID == "" {
			return
		}

		h(ctx)
	}
}
