package middlewares

import "github.com/Necroforger/dgrouter/exrouter"

// Typing triggers a Typing event
func Typing(h exrouter.HandlerFunc) exrouter.HandlerFunc {
	return func(ctx *exrouter.Context) {
		ctx.Ses.ChannelTyping(ctx.Msg.ChannelID)
		h(ctx)
	}
}
