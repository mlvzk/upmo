package smartlink

// URL contains info from a URL
type URL struct {
	Canary    bool
	GuildID   string
	ChannelID string
	MessageID string
}

// ParseURL parses the URL into a smartlink
func ParseURL(url string) *URL {
	ss := smartLinkChannelURL.FindAllStringSubmatch(url, -1)
	if ss == nil {
		return nil
	}

	if len(ss) == 0 {
		return nil
	}

	if len(ss[0]) != 5 {
		return nil
	}

	return &URL{
		Canary:    ss[0][1] == "canary.",
		GuildID:   ss[0][2],
		ChannelID: ss[0][3],
		MessageID: ss[0][4],
	}
}
