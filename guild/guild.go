package guild

import (
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
)

const (
	muteChannel = "mute_channel"
)

var (
	// TablePrefix is the variable to prepend to table names
	TablePrefix = ""
)

// Guild is the struct for a guild with its table
type Guild struct {
	db   *storm.DB
	node storm.Node
	s    *discordgo.Session
}

// NewGuild initializes a new Guild
func NewGuild(db *storm.DB, s *discordgo.Session) (*Guild, error) {
	guild := &Guild{
		db:   db,
		node: db.From("guilds"),
		s:    s,
	}

	return guild, nil
}
