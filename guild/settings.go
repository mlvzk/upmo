package guild

// Settings is the struct for a guild's settings
type Settings struct {
	GuildID string

	MutedChannel string
	LogChannel   string
}
