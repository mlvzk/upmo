package tags

import "github.com/asdine/storm"

func tx(n string, f func(storm.Node) error) error {
	node, err := db.From(n).Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := f(node); err != nil {
		return err
	}

	return node.Commit()
}
