package tags

import (
	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
)

func get(ctx *exrouter.Context) {
	g, err := ctx.Guild(ctx.Msg.GuildID)
	if err != nil {
		ctx.Reply("Error fetching guild: " + err.Error())
		return
	}

	t, err := GetTag(g, ctx.Args.After(1))
	if err != nil {
		ctx.Reply("Error: " + err.Error())
		return
	}

	var displayName string

	m, err := ctx.Member(g.ID, t.Author)
	if err != nil {
		displayName = t.Author
	} else {
		displayName = m.User.Username + "#" + m.User.Discriminator

		if m.Nick != "" {
			displayName += " (" + m.Nick + ")"
		}
	}

	embed := &discordgo.MessageEmbed{
		Description: t.Content,
		Author: &discordgo.MessageEmbedAuthor{
			Name:    ctx.Msg.Author.Username + "#" + ctx.Msg.Author.Discriminator,
			IconURL: ctx.Msg.Author.AvatarURL("32"),
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text:    "Tag " + t.Name + " from " + displayName,
			IconURL: m.User.AvatarURL("32"),
		},
	}

	if m, err := ctx.Member(g.ID, ctx.Msg.Author.ID); err == nil && m.Nick != "" {
		embed.Author.Name = m.Nick + " (" + embed.Author.Name + ")"
	}

	if len(t.Attachments) > 0 {
		embed.Image = &discordgo.MessageEmbedImage{
			URL: t.Attachments[0],
		}

		for i := 1; i < len(t.Attachments); i++ {
			embed.Description += "\n" + t.Attachments[i]
		}
	}

	// Delete the old message
	ctx.Ses.ChannelMessageDelete(ctx.Msg.ChannelID, ctx.Msg.ID)

	_, err = ctx.Ses.ChannelMessageSendEmbed(ctx.Msg.ChannelID, embed)
	if err != nil {
		logger.Error(ctx.Ses, ctx.Msg.GuildID, err)
	}
}
