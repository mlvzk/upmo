package tags

import (
	"time"

	"github.com/Necroforger/dgrouter/exmiddleware"
	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
)

const help = `Usage:
    tags get <name>
    tags set <name> <content>`

// Route is the plug-in router for Nix
func Route(rt *exrouter.Route) {
	rt.Cat("Tags")
	rt.Use(Check)
	rt.Use(helpfn)
	rt.Use(exmiddleware.UserCooldown(
		3*time.Second,
		func(ctx *exrouter.Context) {},
	))

	rt.On("get", get).Desc("Get a tag")
	rt.On("set", set).Desc("Set a tag")
	rt.On("del", del).Desc("Delete a tag")
}

func helpfn(h exrouter.HandlerFunc) exrouter.HandlerFunc {
	return func(ctx *exrouter.Context) {
		if len(ctx.Args) <= 1 {
			ctx.Reply(help)
			return
		}

		h(ctx)
	}
}

func tagUserAllowed(ctx *exrouter.Context, t *Tag) (bool, error) {
	p, err := ctx.Ses.State.UserChannelPermissions(
		ctx.Msg.Author.ID, ctx.Msg.ChannelID,
	)

	if err != nil {
		return false, err
	}

	if t.Author != ctx.Msg.Author.ID &&
		p&discordgo.PermissionAdministrator == 0 {
		return false, nil
	}

	return true, nil
}
