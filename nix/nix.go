package nix

import (
	"log"

	"github.com/Necroforger/dgrouter/exrouter"
	"gitlab.com/diamondburned/nixpkgs-db"
)

var stateError error

// Initialize initializes the Nixpkgs database
func Initialize() {
	go func() {
		if err := nixpkgs.Initialize(true); err != nil {
			stateError = err
			return
		}

		log.Println("Finished populating the Nixpkgs database.")
	}()
}

// Check checks for error
func Check(h exrouter.HandlerFunc) exrouter.HandlerFunc {
	return func(ctx *exrouter.Context) {
		if stateError != nil {
			ctx.Reply("Nix error: " + stateError.Error())
			return
		}

		h(ctx)
	}
}
