package choose

import (
	"math/rand"
	"time"
)

func init() {
	// Ensures randomness
	rand.Seed(time.Now().UnixNano())
}
