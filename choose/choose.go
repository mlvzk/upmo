package choose

import (
	"errors"
	"math/rand"
	"strings"
	"unicode"
)

const (
	delimiter = " or "
)

var (
	errOnlyOne = errors.New("Only one options provided")
)

// Eval chooses
func Eval(input string) (string, error) {
	choices := strings.Split(input, delimiter)
	if len(choices) < 2 {
		return "", errOnlyOne
	}

	return strings.TrimSpace(capitalize(choices[rand.Intn(len(choices))])) + ".", nil
}

func capitalize(s string) string {
	b := false
	return strings.Map(func(r rune) rune {
		if !b {
			b = true
			return unicode.ToUpper(r)
		}

		return r
	}, s)
}
