package wheel

import (
	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
)

// Action is an interface to plug in a module.
type Action interface {
	Initialize(db *storm.DB) error
	Name() string
	// Admin-only (UserisAdmin)
	Setup(exrouter.Context, string) error
	// node.From(Guild.ID).From(Action.Name())
	Act(*discordgo.Session, *Vote) (string, error)
}
