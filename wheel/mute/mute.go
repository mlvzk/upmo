package mute

import (
	"errors"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/wheel"
)

const (
	moduleName = "mute"
)

var (
	db *storm.DB
)

// Mute is the interface-pluggable struct containing methods
type Mute struct {
	GuildID  string `storm:"unique,id"`
	MuteRole string

	MutedUsers []string
}

// Initialize sets the global database
func (m *Mute) Initialize(d *storm.DB) error {
	db = d
	return nil
}

// Name returns the module name
func (m *Mute) Name() string {
	return moduleName
}

// Act acts on the user when the voting threshold was reached. The vote is ended and the logs are finalized.
// `node` is db.From("mute")
func (m *Mute) Act(d *discordgo.Session, vote *wheel.Vote) (string, error) {
	// Grab the latest Mute entry from the database
	if err := node.One("GuildID", vote.GuildID, m); err != nil {
		return "", err
	}

	// Get the guild member
	m, err := d.State.Member(ctx.Msg.GuildID, ctx.Msg.Author.ID)
	if err != nil {
		m, err = d.GuildMember(ctx.Msg.GuildID, ctx.Msg.Author.ID)
		if err != nil {
			return "", err
		}
	}

	// Give him the Muted role
	if err := d.GuildMemberRoleAdd(
		vote.GuildID,
		vote.TargetUser,
		// This relies on the Setup() method
		m.MuteRole,
	); err != nil {
		return "", err
	}

	// Start the transaction as rw
	if err := openStore(func(node storm.Node) error {
		// Add the muted user to the store
		m.MutedUsers = append(m.MutedUsers, vote.TargetUser)

		// Save the database
		if err := store.Save(m); err != nil && err != storm.ErrAlreadyExists {
			return err
		}

		return nil

	}); err != nil {
		return err
	}

	var name = m.Nick
	if m.Nick == "" {
		name = m.User.Username
	}

	// Return the muted message
	return "Muted " + name + " (userID: `" + m.User.ID + "`)", nil
}

// Setup sets up the module
func (m *Mute) Setup(ctx *exrouter.Context, args string) error {
	if args == "" {
		return errors.New("Missing mute channel")
	}

	rls, err := ctx.Ses.GuildRoles(ctx.Msg.GuildID)
	if err != nil {
		return err
	}

	var role *discordgo.Role

	for _, rl := range rls {
		if rl.ID == args || rl.Name == args {
			role = rl
			break
		}
	}

	if role == nil {
		return errors.New("Role not found")
	}

	return openStore(func(node storm.Node) error {
		m.GuildID = ctx.Msg.GuildID
		m.MuteRole = role.ID

		if err := store.Save(m); err != nil && err != storm.ErrAlreadyExists {
			return err
		}

		return nil
	})
}

// helper fn
func (m *Mute) openStore(f func(*storm.Node) error) error {
	node, err := db.From(m.Name()).Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := f(node); err != nil {
		return err
	}

	return node.Commit()
}
