package logger

import "github.com/bwmarrin/discordgo"

// Settings contains the channel to log into
type Settings struct {
	GuildID    string `storm:"unique,id"`
	LogChannel string `storm:"unique"`
}

// Setup sets up or overrides an existing setting
func Setup(d *discordgo.Session, guildID, channelID string) error {
	g, err := d.State.Guild(guildID)
	if err != nil {
		return err
	}

	c, err := d.State.Channel(channelID)
	if err != nil {
		return err
	}

	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	s := Settings{
		GuildID:    g.ID,
		LogChannel: c.ID,
	}

	node.Save(&s)

	return node.Commit()
}
