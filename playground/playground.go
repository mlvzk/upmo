// Package playground provides wrappers for various playgrounds for languages.
package playground

import (
	"strings"
)

// Playground is the interface for executing playgrounds
type Playground interface {
	// Execute runs the code in the playground
	Execute(string) error

	// ExecuteShorthand executes the code with boilerplate code.
	// The function takes in (prefix, body)
	ExecuteShorthand(string, string) error

	// GetOutput returns the code output
	GetOutput() string

	// GetErrors returns the playgrond error
	GetErrors() string
}

// Execute executes the code using the playground
func Execute(p Playground, code string, shorthand bool) (string, string) {
	var err error

	if shorthand {
		lines := strings.Split(code, "\n")
		body := make([]string, 0, len(lines))
		head := make([]string, 0, len(lines))

		for _, l := range lines {
			l = strings.TrimSpace(l)

			if len(l) > 0 && l[0] == ':' {
				head = append(head, l[1:])
			} else {
				body = append(body, l)
			}
		}

		err = p.ExecuteShorthand(
			strings.Join(head, "\n"),
			strings.Join(body, "\n"),
		)
	} else {
		err = p.Execute(code)
	}

	if err != nil {
		return "", err.Error()
	}

	return p.GetOutput(), p.GetErrors()
}
