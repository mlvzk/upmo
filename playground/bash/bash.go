package bash

import (
	"encoding/json"
	"net/url"

	"gitlab.com/diamondburned/upmo/playground/client"
)

const (
	// Compile is the constant to the compile URL
	Compile = "https://rextester.com/rundotnet/Run"
)

// Bash is the struct containing methods for playgrounds
type Bash struct {
	Errors   string
	Files    string
	Result   string
	Stats    string
	Warnings string
}

// ExecuteShorthand executes the code with boilerplate code
func (b *Bash) ExecuteShorthand(prefix, input string) error {
	return b.Execute(prefix + "\n" + input)
}

// Execute runs the code in the playground
func (b *Bash) Execute(input string) error {
	req, err := client.Post(Compile, url.Values{
		"LanguageChoiceWrapper": {"38"},
		"EditorChoiceWrapper":   {"3"},
		"LayoutChoiceWrapper":   {"1"},
		"Program":               {input},
		"Input":                 {""},
		"Privacy":               {""},
		"PrivacyUsers":          {""},
		"Title":                 {""},
		"SavedOutput":           {""},
		"WholeError":            {""},
		"WholeWarning":          {""},
		"StatsToSave":           {""},
		"CodeGuid":              {""},
		"IsInEditMode":          {"False"},
		"IsLive":                {"False"},
	})

	if err != nil {
		return err
	}

	defer req.Body.Close()

	return json.NewDecoder(req.Body).Decode(&b)
}

// GetOutput returns the code output
func (b *Bash) GetOutput() string {
	if b.Warnings != "" {
		return b.Warnings + "\n" + b.Result
	}

	return b.Result
}

// GetErrors returns the playgrond error
func (b *Bash) GetErrors() string {
	if b.Errors != "" {
		return b.Errors + "\n" + b.Warnings
	}

	return ""
}
