package bash

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestBash(t *testing.T) {
	code := `echo Renzix gay`

	b := &Bash{}
	if err := b.ExecuteShorthand("", code); err != nil {
		t.Fatal(err)
	}

	if b.GetOutput() != "Renzix gay\n" {
		t.Fatal("Output doesn't match: " + b.GetOutput())
	}

	if err := b.ExecuteShorthand("", code[1:] /*Emulate error*/); err != nil {
		t.Fatal(err)
	}

	spew.Dump(b)
}
