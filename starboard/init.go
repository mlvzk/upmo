package starboard

import "github.com/asdine/storm"

var (
	db   *storm.DB
	node storm.Node
)

// Initialize sets up the library for use
func Initialize(d *storm.DB) {
	db = d
	node = db.From("starboard")
	node.Init(&Settings{})
}
