package starboard

import "github.com/bwmarrin/discordgo"

func emojiInReactions(rs []*discordgo.MessageReactions, rxn string) *discordgo.MessageReactions {
	for _, r := range rs {
		if r.Emoji != nil && r.Emoji.Name == rxn {
			return r
		}
	}

	return nil
}
