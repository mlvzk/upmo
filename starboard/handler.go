package starboard

import (
	"fmt"
	"strings"
	"time"

	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
)

var everyoneEscaper = strings.NewReplacer(
	"@everyone", "@\u200be\u200bver\u200byo\u200bne",
	"@here", "@\u200bh\u200ber\u200be",
)

// Handler serves as a plug-in for the Discordgo Event Handler.
// This function handles everything in the database, easing out the library's
// usage. The function will return if Initialize() is not called.
func Handler(d *discordgo.Session, r *discordgo.MessageReactionAdd) {
	if err := handler(d, r); err != nil {
		logger.Error(d, r.GuildID, err)
	}
}

func handler(d *discordgo.Session, r *discordgo.MessageReactionAdd) error {
	if db == nil {
		return errUnitialized
	}

	// Get message from either the state cache or the channel
	m, err := d.State.Message(r.ChannelID, r.MessageID)
	if err != nil {
		m, err = d.ChannelMessage(r.ChannelID, r.MessageID)
		if err != nil {
			return err
		}
	}

	t, err := m.Timestamp.Parse()
	if err != nil {
		return err
	}

	// 48 hours == 2 days
	// Drop all messages older than 2 days
	if t.Add(time.Hour * 48).Before(time.Now()) {
		return nil
	}

	// If it's not in a guild, ignore it
	if r.GuildID == "" {
		return nil
	}

	var s Settings

	// Check if the guild is in the database
	if err := node.One("GuildID", r.GuildID, &s); err != nil {
		// Drop if it's not, it's not our business
		return nil
	}

	for _, c := range s.Blacklist {
		if c == r.ChannelID {
			// Channel is blacklisted, removing
			return nil
		}
	}

	// If the emoji matches the star and the reaction is
	// from the message author
	if r.Emoji.Name == s.Reaction && r.UserID == m.Author.ID {
		// we delete it

		// Error is not caught intentionally
		d.MessageReactionRemove(
			/* ChannelID */ r.ChannelID,
			/* MessageID */ r.MessageID,
			/* EmojiID */ s.Reaction,
			/* UserID */ m.Author.ID,
		)

		return nil
	}

	rxn := emojiInReactions(m.Reactions, s.Reaction)
	if rxn == nil { // Emoji is not in the message
		return nil
	}

	// If the reaction count is not equal, drop it
	if rxn.Count < int(s.Starcount) {
		return nil
	}

	// Opens the bucket for this guild
	gdb, err := node.From(s.WebhookChannel).Begin(true)
	if err != nil {
		return err
	}

	defer gdb.Rollback()

	var mdb Message

	// Query the database for the message's ID
	if err := gdb.One("ID", r.MessageID, &mdb); err != storm.ErrNotFound {
		// If the error is NOT not found, drop it.
		// `err` is returned, since if the message is really in the
		// database, there would be no error (nil). Any other error
		// is fatal.
		return err
	}

	// By this point, we're certain that the message should be
	// starred, as it satisfies the conditions that the emoji
	// and the count match what we want.

	// Save the message
	mdb = Message{
		ID:        r.MessageID,
		ChannelID: r.ChannelID,
		GuildID:   r.GuildID,
	}

	// Save it into the database
	if err := gdb.Save(&mdb); err != nil {
		// This shouldn't happen, unless the error is an
		// actual error.
		return err
	}

	var nickname = m.Author.Username

	// Doing all this for a string of nickname
	if m, err := d.State.Member(r.GuildID, m.Author.ID); err == nil {
		if m.Nick != "" {
			nickname = m.Nick
		}
	}

	var embeds = make([]*discordgo.MessageEmbed, 0, len(m.Attachments))
	for _, a := range m.Attachments {
		if a.Width > 0 && a.Height > 0 {
			embeds = append(embeds, &discordgo.MessageEmbed{
				Image: &discordgo.MessageEmbedImage{
					URL:      a.URL,
					ProxyURL: a.ProxyURL,
					Width:    a.Width,
					Height:   a.Height,
				},
			})
		}
	}

	webhook := &discordgo.WebhookParams{
		Username: nickname,
		Content: fmt.Sprintf(
			// message (source)
			"%s [*(source)*](<https://discordapp.com/channels/%s/%s/%s>)",
			everyoneEscaper.Replace(m.Content), // "escapes" @everyone mentions
			r.GuildID, r.ChannelID, r.MessageID,
		),
		AvatarURL: m.Author.AvatarURL("512"),
		Embeds:    embeds,
	}

	if err := d.WebhookExecute(s.WebhookID, s.WebhookToken, true, webhook); err != nil {
		return err
	}

	return gdb.Commit()
}
