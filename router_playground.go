package main

import (
	"regexp"
	"strings"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/playground"
)

var (
	markdownLanguageRegex = regexp.MustCompile("^```(\\S+)\\n")
	codeblockDeescaper    = strings.NewReplacer(
		"```", "`\u200b`\u200b`\u200b",
	)
)

func routerPlayground(ctx *exrouter.Context) {
	var lang, code string

	switch firstArg := ctx.Args.Get(1); {
	case firstArg == "":
		sendToDiscord(ctx, "Usage: \n```\nplayground `\u200b``[lang]\n:prefix statement\ncode\n`\u200b`\u200b`\n```")
		return
	case strings.HasPrefix(firstArg, "```"):
		code = getRestOfSlice(ctx, 1)
		m := markdownLanguageRegex.FindAllStringSubmatch(code, -1)

		if m != nil && len(m) > 0 {
			lang = m[0][1]
			code = strings.TrimPrefix(code, m[0][0])
		}

		code = strings.Trim(code, "`")
	default:
		lang = firstArg
		code = strings.Trim(getRestOfSlice(ctx, 2), "`")
	}

	p := playground.ReflectLanguage(playground.Language(lang))
	if p == nil {
		sendToDiscord(ctx, "No matching languages.")
		return
	}

	e := &discordgo.MessageEmbed{}

	if output, err := playground.Execute(p, code, true); err != "" {
		e.Color = 0xFF0000
		e.Title = "Error!"
		e.Description = "```\n" + codeblockDeescaper.Replace(err) + "\n```"
	} else {
		e.Color = 0x00FF00
		e.Description = "```\n" + codeblockDeescaper.Replace(output) + "\n```"
	}

	sendToDiscord(ctx, e)
}

func getRestOfSlice(ctx *exrouter.Context, i int) string {
	fields := strings.Split(ctx.Msg.Content, " ")
	if i > len(fields) {
		return ""
	}

	return strings.Join(fields[i:], " ")
}
