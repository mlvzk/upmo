package main

import (
	"bytes"
	"fmt"
	"image"
	"strings"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/Necroforger/dgwidgets"
	"github.com/bwmarrin/discordgo"
	"github.com/disintegration/imaging"
	"gitlab.com/diamondburned/crawl-sticker/stickers"
	"gitlab.com/diamondburned/upmo/compresspng"
	"gitlab.com/diamondburned/upmo/logger"
)

func stickersHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	words := strings.Fields(m.Content)

	var sticker *stickers.Sticker

	for _, w := range words {
		if w[0] == '\\' {
			continue
		}

		// :n: len 3, we need to trim the colons
		if len(w) < 3 {
			continue
		}

		// If not :n:
		if !(w[0] == ':' && w[len(w)-1] == ':') {
			continue
		}

		sticker = stickers.GetSticker(w)
		if sticker == nil {
			continue
		}

		break
	}

	if sticker == nil {
		return
	}

	b, err := compresspng.CompressFile(
		sticker.Path,
		func(i image.Image) image.Image {
			return imaging.Fit(i, 150, 150, imaging.CatmullRom)
		},
	)

	if err != nil {
		logger.Error(s, m.ChannelID, err)
		return
	}

	_, err = s.ChannelMessageSendComplex(m.ChannelID, &discordgo.MessageSend{
		File: &discordgo.File{
			Name:        sticker.Name + ".png",
			ContentType: "image/png",
			Reader:      bytes.NewReader(b),
		},
	})

	if err != nil {
		logger.Error(s, m.ChannelID, err)
		return
	}
}

func routerStickers(rt *exrouter.Route) {
	rt.Cat("Telegram Stickers")

	rt.On("stickers", routerStickersQuery).
		Desc("List all stickersets or all stickers in one")
	rt.On("sticker", routerStickersSearch).
		Desc("Searches all stickers")
}

func routerStickersSearch(ctx *exrouter.Context) {
	var name = ctx.Args.Get(1)
	if name == "" {
		sendToDiscord(ctx, "No stickers found")
		return
	}

	ss := stickers.SearchStickers(name)
	if ss == nil || len(ss) == 0 {
		sendToDiscord(ctx, "No stickers found")
		return
	}

	p := routerStickersPaginate(
		ctx, fmt.Sprintf("Found %d stickers", len(ss)), ss,
	)

	if err := p.Spawn(); err != nil {
		errToDiscord(ctx, err)
	}
}

func routerStickersQuery(ctx *exrouter.Context) {
	var all = stickers.GetAllStickersets()
	var s strings.Builder
	var setName = ctx.Args.Get(1)

	if setName == "" {
		for i, ss := range all {
			s.WriteString(fmt.Sprintf(
				"%s - %d stickers",
				ss.Name, len(ss.Stickers),
			))

			if i != len(all)-1 {
				s.WriteByte('\n')
			}
		}

		sendToDiscord(ctx, &discordgo.MessageEmbed{
			Title:       "List of stickersets",
			Description: s.String(),
		})

		return
	}

	setName = strings.ToLower(setName)

	var set *stickers.Stickerset
	for _, ss := range all {
		if strings.ToLower(ss.Name) == setName {
			set = ss
			break
		}
	}

	if set == nil {
		sendToDiscord(ctx, "Unknown sticker set.")
		return
	}

	p := routerStickersPaginate(
		ctx, "List of "+set.Name+" stickers",
		set.Stickers,
	)

	if err := p.Spawn(); err != nil {
		errToDiscord(ctx, err)
	}
}

func routerStickersPaginate(
	ctx *exrouter.Context,
	title string, stickers []*stickers.Sticker,
) *dgwidgets.Paginator {

	p := newPaginator(ctx)
	p.Pages = make(
		[]*discordgo.MessageEmbed,
		0, len(stickers)/10+1,
	)

	for j := 0; j < len(stickers); j += 10 {
		embed := &discordgo.MessageEmbed{
			Title: title,
		}

		for i := j; i-j < 10 && i < len(stickers); i++ {
			embed.Description += ":" + stickers[i].Name + ":\n"
		}

		p.Pages = append(p.Pages, embed)
	}

	p.SetPageFooters()
	return p
}
