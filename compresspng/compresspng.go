// Package compresspng provides easy functions to compressing
// PNG images.
package compresspng

import (
	"bytes"
	"errors"
	"image"
	"image/png"
	"os"
	"sync"
)

var (
	compressCache = map[string][]byte{}
	compressMutex = &sync.Mutex{}

	enc *png.Encoder
)

// CompressionLevel is the compression level used for all PNG images
var CompressionLevel = png.BestCompression

// ImageMiddleware is used to process the image before compressing.
// This will be cached as usual.
type ImageMiddleware func(image.Image) image.Image

// MiddlewareError is the error returned when a middleware returns nil.
// The function halts.
var MiddlewareError = errors.New("One of the middlewares failed")

// CompressFile compresses the image from a file
func CompressFile(name string, ms ...ImageMiddleware) ([]byte, error) {
	compressMutex.Lock()
	defer compressMutex.Unlock()

	if enc == nil {
		enc = &png.Encoder{
			CompressionLevel: CompressionLevel,
		}
	}

	if b, ok := compressCache[name]; ok {
		return b, nil
	}

	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		return nil, err
	}

	return compress(name, img, ms...)
}

// CompressImage compresses an image and, if key != "", stores it in the cache.
func CompressImage(key string, img image.Image, ms ...ImageMiddleware) ([]byte, error) {
	compressMutex.Lock()
	defer compressMutex.Unlock()

	if key != "" {
		if b, ok := compressCache[key]; ok {
			return b, nil
		}
	}

	return compress(key, img, ms...)
}

func compress(key string, img image.Image, ms ...ImageMiddleware) ([]byte, error) {
	for _, m := range ms {
		if img = m(img); img == nil {
			return nil, MiddlewareError
		}
	}

	if enc == nil {
		enc = &png.Encoder{
			CompressionLevel: CompressionLevel,
		}
	}

	var b bytes.Buffer
	if err := png.Encode(&b, img); err != nil {
		return nil, err
	}

	bytes := b.Bytes()

	if key != "" {
		compressCache[key] = bytes
	}

	return bytes, nil
}

// ClearCache wipes the image cache
func ClearCache() {
	compressMutex.Lock()
	defer compressMutex.Unlock()

	compressCache = map[string][]byte{}
}
