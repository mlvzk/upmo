package metrics

// Reset resets the metrics for one user. False is returned
// if nothing was changed, eg user was nil
func (m *Metrics) Reset(guildID string, userIDs ...string) error {
	tx, err := m.node.From(guildID).Begin(true)
	if err != nil {
		return err
	}

	defer tx.Rollback()

	for _, userID := range userIDs {
		tx.Save(m.GetUserDefault(userID))
	}

	return tx.Commit()
}

// DropGuild drops the whole guild's metrics`
func (m *Metrics) DropGuild(guildID string) error {
	tx, err := m.node.Begin(true)
	if err != nil {
		return err
	}

	defer tx.Rollback()

	if err := tx.Drop(guildID); err != nil {
		return err
	}

	return tx.Commit()
}
