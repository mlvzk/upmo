package main

import (
	"runtime"
	"runtime/debug"
	"time"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/bwmarrin/discordgo"
)

func routerGC(ctx *exrouter.Context) {
	e := &discordgo.MessageEmbed{
		Title: "Go debug info",
		Fields: []*discordgo.MessageEmbedField{
			embedMakeField("Number of goroutines", runtime.NumGoroutine(), false),
			embedMakeField("GOMAXPROCS", runtime.GOMAXPROCS(-1), true),
			embedMakeField("GOOS", runtime.GOOS, true),
			embedMakeField("GOARCH", runtime.GOARCH, true),
			embedMakeField("Go version", runtime.Version(), false),
		},
	}

	var gc = &debug.GCStats{}
	debug.ReadGCStats(gc)

	if gc != nil {
		e.Fields = append(e.Fields,
			embedMakeField("Last garbage collection", gc.LastGC.Format(time.Kitchen), false),
			embedMakeField("Total garbage collections", gc.NumGC, false),
			embedMakeField("Total pauses for GC", gc.PauseTotal, false),
		)
	}

	sendToDiscord(ctx, e)

	runtime.GC()
}
